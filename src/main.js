import Vue from 'vue'
import App from './App.vue'

import hljs from 'highlight.js'
import 'highlight.js/styles/railscasts.css'

Vue.directive('hljs', el => {
    let blocks = el.querySelectorAll('pre')
    Array.prototype.forEach.call(blocks, hljs.highlightBlock)
})


import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

// 高德的map
import VueAMap from 'vue-amap';
Vue.use(VueAMap);

// 初始化vue-amap，高德的key值需要自己去申请
VueAMap.initAMapApiLoader({
    key: 'cfd8da5cf010c5f7441834ff5e764f5b',
    plugin: ['AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PlaceSearch', 'AMap.Geolocation', 'AMap.Geocoder'],
    v: '1.4.4',
    uiVersion: '1.0'
});

Vue.config.productionTip = false

new Vue({
    render: h => h(App)
}).$mount('#app')
